﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Admin.Directory.directory_v1;
using Google.Apis.Admin.Directory.directory_v1.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using System.IO;
using System.Threading;
using data;

namespace api.Controllers
{

    [ApiController]
    public class UsuariosController : ControllerBase
    {
        public string _credencial = "credentials.json";
        public string _token = "token.json";
        
        // GET api/values
        [HttpGet]
        [Route("api/Usuarios")]
        public  ActionResult<Users> Get()
        {
            UsuariosService usrServ = new UsuariosService(_credencial, _token);
            Users usuarios = usrServ.GetUsuarios();
            return usuarios;
        }

        // GET api/values/mail@mail.com/validar
        [HttpGet("{email}")]
        [Route("api/Usuarios/{email}/validar")]
        public ActionResult<RespuestaValidarUsuario> Get(string email)
        {
            RespuestaValidarUsuario resp = new RespuestaValidarUsuario();
            UsuariosService usrServ = new UsuariosService(_credencial, _token);
            return usrServ.Valida(email);
            
        }

        // POST api/values
        [HttpPost]
        [Route("api/Usuarios")]

        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        [Route("api/Usuarios")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        [Route("api/Usuarios")]
        public void Delete(int id)
        {
        }
    }
}
