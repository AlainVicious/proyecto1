using Google.Apis.Auth.OAuth2;
using Google.Apis.Admin.Directory.directory_v1;
using Google.Apis.Admin.Directory.directory_v1.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Linq;

namespace data
{
    public class UsuariosService
    {
        private string[] Scopes = { DirectoryService.Scope.AdminDirectoryUserReadonly };
        private string ApplicationName = "Directory API .NET Quickstart";
        private string _credencial { get; set; }
        private string _token { get; set; }

        private DirectoryService _service { get; set; }
        private FileStream _stream { get; set; }

        public UsuariosService(string credencial, string token)
        {
            this._credencial = credencial;
            this._token = token;
            this._stream = new FileStream(_credencial, FileMode.Open, FileAccess.Read);
            string credPath = _token;

            UserCredential credential;
            credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                GoogleClientSecrets.Load(_stream).Secrets,
                Scopes,
                "user",
                CancellationToken.None,
                new FileDataStore(credPath, true)).Result;
            Console.WriteLine("Credential file saved to: " + credPath);

            this._service = new DirectoryService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = ApplicationName,
            });

        }
        public Users GetUsuarios()
        {
            UsersResource.ListRequest request = _service.Users.List();
            request.Customer = "my_customer";
            request.MaxResults = 100;
            request.OrderBy = UsersResource.ListRequest.OrderByEnum.Email;

            // List users.
            Users users = request.Execute();

            //Console.Read();
            return users;
        }



        public RespuestaValidarUsuario Valida(string email)
        {
            RespuestaValidarUsuario resp = new RespuestaValidarUsuario();

            UsersResource.ListRequest request = _service.Users.List();
            request.Customer = "my_customer";
            request.MaxResults = 10;
            request.OrderBy = UsersResource.ListRequest.OrderByEnum.Email;

            IList<User> usuarios = request.Execute().UsersValue;
            var usuario = usuarios.FirstOrDefault(c => c.PrimaryEmail == email);
            if (usuario == null)
            {
                return resp;
            }
            else
            {
                resp.Existe = true;
                if (usuario.Organizations != null)
                {
                    resp.PerteneceOrganizacion = true;
                }
                return resp;
            }
        }


    }

}