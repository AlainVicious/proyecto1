using System;
namespace data
{
    public class RespuestaValidarUsuario
    {
        public bool PerteneceOrganizacion { get; set; }
        public bool Existe { get; set; }
        public RespuestaValidarUsuario()
        {
            this.PerteneceOrganizacion = false;
            this.Existe = false;
        }
    }

}